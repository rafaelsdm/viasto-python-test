# Viasto Python Test

## Requirements
* Vagrant
* Ubuntu or Debian 64bits (this project was not tested on Mac OS X and Windows)
* TCP Port 21001 free on host (can be changed in `scripts/envs.sh`)

## About this project

This project allows for users send notifications via API to another group of users or to anyone in the network.

Messages may be send to none, one or many groups, in case of sending to none group, it must be set as public and hence, anyone with access to it may read the messages.

Messages also have a level and this level have priority. Right now, priority don't serve any purpose. In the future it may be used to filter down messages, in the same sense as `logging.INFO` and `logging.ERROR` are used to filter logs.

No default level or groups are created, you must first create them on Django admin.

Groups are created at `Auth -> Groups`.

Levels are created at `Messenger -> Levels`.

When accessing via vagrant, a default user is created with credentials `admin` and password `1234`. 

* Developed and tested on Chrome, might work on Firefox
* Exempt of jQuery

## Usage and Notes

All scripts are located in `scripts/`

Any command outside `scripts/` folder requires `envs.sh` to be loaded using the `source` command or manually exporting `MSGR_PORT` and `src`:

    $ source scripts/envs.sh
    
Or

    $ export MSGR_PORT=21001
    $ export src={PROJECT_ROOT}/src

The `envs.sh` file will create a virtualenv in the host so you can use the project outside the VM.

### Directory structure

    + viasto-python-test
      + scripts/ -> Helper scripts
      + src/ -> Python code
      + vm/ -> Vagrant configuration and vm related files

### Django Settings

This project allows for custom settings files besides the default `settings.py`.

In order to create a custom settings, create a new file on `src/messenger/settings/` with any (python module compatible) name and export it as following:

    $ export DJANGO_SETTINGS_MODULE='messenger.settings.YOURSETTINGS'

The default settings file used is `prod.py`, it uses MariaDB/MySQL as backend with unsecure default password. You may want to change it before deploying to production.

### Starting the VM


To start the container, run (from the project root directory):

    $ ./scripts/startup.sh

This will start the VM and setup everything on it (Apache config, virtualenv, etc) and open the port 21001 on the host to access the project.

If you have exported `MSGR_PORT` and `src` you can also start the VM manually:
    
    $ cd vm/
    $ vagrant up --provider virtualbox
    
Now you can visit http://localhost:21001/

## Sending messages

To send messages, you can use the script located in `scripts/post-message.py`, it uses pure-python code, there is no need to have Django or other software installed.

You can copy `post-message.py` to anywhere in you `$PATH`, there is no requirements.

Run `post-message.py --help` to learn about all parameters.

## API

This API uses a simple token-based authentication. 

`HTTP Basic` authentication is not accepted.

The token is first generated at registration and can be regenerated in the dashboard.

All POST requests must include a `Authorization: Token {TOKEN}` HTTP header.

### Endpoints

* `/messages/` - Lists and creates messages

    To create messages, you must send a JSON with 'content', 'level' and ('group_target' or 'is_public')

    Examples: 

        {"content": "Foo", "level": "bar", "is_public": true}
        {"content": "Foo", "level": "bar", "group_target": ["group 1"]}

* `/groups/` - Lists all available groups
* `/levels/` - Lists all available levels

You must first get the list of available groups and levels prior to send them to the message.

## Known issues and improvements

* Notifications are updated using timed JSON requests to the API (every 3 seconds), this should be done using WebSockets, so the server could actually push the message to browser
* There is no way to send messages to individuals, this is not a chat system
* Login and registration pages are pretty much only a form, there is no UI