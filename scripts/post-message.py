#!/usr/bin/env python
import json
import argparse
import socket
import urllib2
import sys

SERVER = 'localhost:21001'


def _get_response(url, token=None, data=None):
    request = urllib2.Request(url, data)
    if token:
        request.add_header('Authorization', 'Token {}'.format(token))

    if data:
        request.add_header('Accept', 'application/json')
        request.add_header('Content-Type', 'application/json')

    try:
        text = urllib2.urlopen(request).read()
    except (urllib2.URLError, urllib2.HTTPError, socket.error), e:
        sys.stderr.write('Error trying to open {}:\n\t{}\n'.format(url, e))
        exit(1)
    else:
        return json.loads(text)


def post_message(message, level, token, is_public=None, group_target=None):
    if group_target:
        group_target = group_target.split(',')

    data = {
        'content': message,
        'level': level
    }

    if is_public:
        data['is_public'] = True
    elif group_target:
        data['group_target'] = group_target

    response = _get_response(
        'http://{}/messages/'.format(SERVER),
        token,
        json.dumps(data)
    )

    if response:
        print 'Message sent'


def list_levels():
    url = 'http://{}/levels/?format=json'.format(SERVER)
    levels = []
    while True:
        response = _get_response(url)
        levels.extend(response['results'])
        if response['next']:
            url = response['next']
        else:
            break
    print 'List of levels:'
    print '\n'.join(['\t{} (priority {})'.format(level['name'], level['priority']) for level in levels])
    print


def list_groups():
    url = 'http://{}/groups/?format=json'.format(SERVER)
    groups = []
    while True:
        response = _get_response(url)
        groups.extend(response['results'])
        if response['next']:
            url = response['next']
        else:
            break
    print 'List of groups:'

    print '\n'.join(['\t{}'.format(group['name']) for group in groups])
    print



if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Post messages to Viasto Messenger')
    parser.add_argument('--server', dest='server', action='store', help='Overrides the default server ({})'.format(SERVER))
    parser.add_argument('--list-levels', action='store_const', const=True, help='Lists all available levels')
    parser.add_argument('--list-groups', action='store_const', const=True, help='Lists all available groups')
    parser.add_argument('--message', action='store', help="Message's content")
    parser.add_argument('--level', action="store", help="Message level")
    parser.add_argument('--public', action='store_const', help='Post message as public', const=True)
    parser.add_argument('--target', action='store', help='Post message to specific group (multiple groups are allowed, separate them with commas)')
    parser.add_argument('--token', action='store', help='Your access token (required when posting)')

    args = parser.parse_args()
    if not (args.list_levels or args.list_groups) and not args.token:
        sys.stderr.write('Parameter --token required when posting messages\n')

    if args.server:
        SERVER = args.server

    if args.list_groups:
        list_groups()

    if args.list_levels:
        list_levels()

    if args.list_levels or args.list_groups:
        exit(0)

    if not args.public and not args.target:
        sys.stderr.write('Messages should either be public or targeted to a group\n')
        exit(1)

    if args.public and args.target:
        sys.stderr.write('Messages should either be public or targeted to a group, not both\n')
        exit(1)


    if not args.message:
        sys.stderr.write('Message content missing\n')
        exit(1)

    if not args.level:
        sys.stderr.write('Message level missing\n')
        exit(1)

    post_message(
        args.message,
        level=args.level,
        group_target=args.target,
        token=args.token,
        is_public=args.public
    )
