#!/bin/bash

# Load the environment variables
source $(dirname $0)/envs.sh

# Go to the VM folder
cd $vm

# Check the status of the VM
not_created=$(vagrant status|grep "default"|grep "not created"|wc -l|tr -d '[[:space:]]')

if [[ $not_created == "1" ]]; then
    echo "VM not created yet, creating..."
    vagrant up --provider virtualbox
fi
