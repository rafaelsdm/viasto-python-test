#!/bin/bash

export VIRTUALENV_PATH=$HOME/env
export SRC=/project

virtualenv $VIRTUALENV_PATH
source $VIRTUALENV_PATH/bin/activate

pip install -r $SRC/requirements.txt

cd $SRC
echo "Sync DB"
python manage.py syncdb --noinput
echo "Migrate"
python manage.py migrate --noinput
echo "Collect Static"
python manage.py collectstatic --noinput

cd /vagrant/
PYTHONPATH=$PYTHONPATH:/project DJANGO_SETTINGS_MODULE=messenger.settings.prod python create-default-user.py