from django.contrib.auth.models import User
from messenger.models import Token

user = User(username='admin', is_staff=True, is_superuser=True)
user.set_password('1234')
user.save()
Token.objects.get_or_create(user=user)