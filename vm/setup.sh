#!/bin/bash

export DEBIAN_FRONTEND=noninteractive

debconf-set-selections <<< 'mariadb-server-5.5 mysql-server/root_password password 123456'
debconf-set-selections <<< 'mariadb-server-5.5 mysql-server/root_password_again password 123456'

#
# Replaces sources.list (you might want to change it)
#
cp /vagrant/conf/sources.list /etc/apt/

aptitude update -y
aptitude install -y apache2 libapache2-mod-wsgi python-pip\
    python-virtualenv rabbitmq-server mariadb-server mariadb-client\
    libmariadb-client-lgpl-dev libmysqlclient-dev python-dev

#
# Apache configuration
#
cp /vagrant/conf/apache-virtualhost.conf /etc/apache2/sites-available/

a2dissite 000-default
a2ensite apache-virtualhost
a2enmod wsgi

service apache2 restart

#
# MariaDB configuration
#
mysql -uroot -p123456 -e "CREATE DATABASE messenger CHARACTER SET utf8"

