# -*- coding: utf-8 -*-
import collections
from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.core.urlresolvers import reverse_lazy
from django.forms.formsets import formset_factory
from django.views.generic.base import TemplateView
from django.views.generic.edit import FormView
from rest_framework import viewsets, permissions, serializers
from messenger.models import Message, Level
from rest_framework.authentication import TokenAuthentication,\
    SessionAuthentication
from rest_framework.serializers import ModelSerializer
from django.db import models
from rest_framework.fields import Field, ReadOnlyField
from django.contrib.auth.models import Group, User
from rest_framework.mixins import RetrieveModelMixin, ListModelMixin,\
    CreateModelMixin

import dateutil.parser

class UserSerializer(ModelSerializer):
    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email')


class GroupSerializer(ModelSerializer):
    class Meta:
        model = Group
        fields = ('name',)


class LevelSerializer(ModelSerializer):
    class Meta:
        model = Level
        fields = ('name', 'priority')


class MessageSerializer(ModelSerializer):
    level = LevelSerializer(read_only=True)
    group_target = GroupSerializer(many=True, read_only=True)
    user = UserSerializer(read_only=True)
    
    class Meta:
        model = Message
        fields = ('created_at', 'group_target', 'level', 'is_public', 'user', 'content')
        
    def create(self, validated_data):
        validated_data['user'] = self.context['request'].user
        try:
            validated_data['level'] = Level.objects.get(name=self.initial_data['level'])
        except Level.DoesNotExist, e:
            raise serializers.ValidationError(u'Invalid level name')
        except KeyError, e:
            raise serializers.ValidationError(u'Level name missing')
        message = Message(**validated_data)

        if 'group_target' in self.initial_data:
            # Coerce group_target into list
            if not isinstance(self.initial_data['group_target'], (list, tuple)):
                self.initial_data['group_target'] = [self.initial_data['group_target']]

            # Validates the groups
            qs = Group.objects.filter(name__in=self.initial_data['group_target'])
            if qs.count() != len(self.initial_data['group_target']):
                # Some group is invalid
                raise serializers.ValidationError(u'Invalid group list')

            message.save()
            message.group_target.add(
                *qs.values_list('id', flat=True)
            )
        else:
            message.save()
        return message


class MessageAPIView(ListModelMixin, RetrieveModelMixin, CreateModelMixin, viewsets.GenericViewSet):
    authentication_classes = (SessionAuthentication, TokenAuthentication)
    queryset = Message.objects.all()
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    serializer_class = MessageSerializer
    
    def get_queryset(self):
        qs = self.queryset
        user = self.request.user
        
        # Anonymous users get to see only public messages
        if user.is_anonymous():
            qs = qs.filter(is_public=True)
            
        # Authenticated users see public messages and those directed to his groups
        if user.is_authenticated():
            qs = qs.filter(
                models.Q(group_target__in=user.groups.all())|models.Q(is_public=True)
            )

        if 'since' in self.request.query_params:
            try:
                dateutil.parser.parse(self.request.query_params['since'])
            except ValueError:
                pass
                # Invalid date format, ignoring
            else:
                qs = qs.filter(
                    created_at__gt=self.request.query_params['since']
                )
        if 'limit' in self.request.query_params:
            try:
                limit = int(self.request.query_params['limit'])
            except ValueError:
                pass
            qs = qs[:limit]
        return qs
    
    
class GroupsAPIView(viewsets.ReadOnlyModelViewSet):
    queryset = Group.objects.all()
    serializer_class = GroupSerializer
    

class LevelAPIView(viewsets.ReadOnlyModelViewSet):
    queryset = Level.objects.filter(is_active=True)
    serializer_class = LevelSerializer
    

class HomeView(TemplateView):
    template_name = 'home.html'


class RegisterView(FormView):
    form_class = UserCreationForm
    template_name = 'register.html'
    success_url = reverse_lazy('register-success')

    def form_valid(self, form):
        form.save()
        return super(RegisterView, self).form_valid(form)