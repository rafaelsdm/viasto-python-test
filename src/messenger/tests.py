# -*- coding: utf-8 -*-

from django.contrib.auth.models import User, Group
from django.core.exceptions import ValidationError
from django.test.client import Client
from django.utils import unittest
from django.core.urlresolvers import reverse
from messenger.models import Level, Message
import json


class ProfileTestCase(unittest.TestCase):
    def test_user_signal(self):
        user = User.objects.get_or_create(username='user')[0]
        self.assertIsNotNone(user.auth_token)
    
    
class ApiTestCase(unittest.TestCase):
    def setUp(self):
        self.user = User.objects.get_or_create(username='user')[0]
        self.token = self.user.auth_token
        self.level = Level.objects.get_or_create(name='lvl')[0]
        self.group = Group.objects.get_or_create(name='grp')[0]

    def test_automatic_set_user_message(self):
        client = Client()
        response = client.post(
            reverse('message-list'), 
            json.dumps({'content': 'test msg', 'level': self.level.name, 'is_public': True}),
            content_type='application/json',
            HTTP_AUTHORIZATION='Token {}'.format(self.token.key),
        )
        data = json.loads(response.content)
        self.assertEqual(data['user']['username'], self.user.username)
        self.assertEqual(data['level']['name'], self.level.name)
        
    def test_target_group(self):
        client = Client()
        response = client.post(
            reverse('message-list'), 
            json.dumps({'content': 'test msg 2', 'level': self.level.name, 'group_target':[self.group.name]}),
            content_type='application/json',
            HTTP_AUTHORIZATION='Token {}'.format(self.token.key),
        )
        data = json.loads(response.content)
        self.assertIn({'name': self.group.name}, data['group_target'])
        # Unless specified, messages must not be public
        self.assertFalse(data['is_public'])

    def test_messages_permission(self):
        client = Client()
        Message.objects.all().delete()

        client.post(
            reverse('message-list'),
            json.dumps({'content': 'test msg 3', 'level': self.level.name, 'group_target':[self.group.name]}),
            content_type='application/json',
            HTTP_AUTHORIZATION='Token {}'.format(self.token.key),
        )
        client.post(
            reverse('message-list'),
            json.dumps({'content': 'test msg 4', 'level': self.level.name, 'is_public': True}),
            content_type='application/json',
            HTTP_AUTHORIZATION='Token {}'.format(self.token.key),
        )
        # Tests anonymous request
        response = client.get(reverse('message-list'))
        data = json.loads(response.content)
        self.assertEqual(data['count'], 1)

        # Tests authenticated request out of the group
        response = client.get(reverse('message-list'), HTTP_AUTHORIZATION='Token {}'.format(self.token.key),)
        data = json.loads(response.content)
        self.assertEqual(data['count'], 1)

        # Tests authenticated request within the group
        self.user.groups.add(self.group)
        response = client.get(reverse('message-list'), HTTP_AUTHORIZATION='Token {}'.format(self.token.key),)
        data = json.loads(response.content)
        self.assertEqual(data['count'], 2)

    def test_invalid_group(self):
        client = Client()
        response = client.post(
            reverse('message-list'),
            json.dumps({'content': 'test msg 5', 'level': self.level.name, 'group_target':['invalid group name']}),
            content_type='application/json',
            HTTP_AUTHORIZATION='Token {}'.format(self.token.key),
        )
        data = json.loads(response.content)
        self.assertIn(u'Invalid group list', data)

    def test_invalid_level(self):
        client = Client()
        response = client.post(
            reverse('message-list'),
            json.dumps({'content': 'test msg 6', 'level': 'invalid level name', 'group_target':[self.group.name]}),
            content_type='application/json',
            HTTP_AUTHORIZATION='Token {}'.format(self.token.key),
        )
        data = json.loads(response.content)
        self.assertIn(u'Invalid level name', data)

    def test_no_level_sent(self):
        client = Client()
        response = client.post(
            reverse('message-list'),
            json.dumps({'content': 'test msg 7', 'group_target':[self.group.name]}),
            content_type='application/json',
            HTTP_AUTHORIZATION='Token {}'.format(self.token.key),
        )
        data = json.loads(response.content)
        self.assertIn(u'Level name missing', data)


    def test_empty_send(self):
        client = Client()
        response = client.post(
            reverse('message-list'),
            '{}',
            content_type='application/json',
            HTTP_AUTHORIZATION='Token {}'.format(self.token.key),
        )
        data = json.loads(response.content)
        self.assertNotIn(u'created_at', data)
