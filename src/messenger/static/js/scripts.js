window.addEventListener('load', function(){
    document.getElementById('my-token').addEventListener('click', function(){
        var div = document.querySelector('.show-my-token');
        if(div.classList.contains('open')){
            div.classList.remove('open');
            div.parentElement.classList.remove('open');
        }else{
            div.classList.add('open');
            div.parentElement.classList.add('open');
        }
    });
})