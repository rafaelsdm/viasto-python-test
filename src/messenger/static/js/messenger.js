(function(){
    var Message = function (){
        // This class handles only the viewing of messages
        this.init = function(){
            console.log("Initializing message system")
            // Load the template
            this.template = document.getElementById('message-template');
            this.template.removeAttribute('id');
            this.template.remove();
            this.insert_before = false;

            // Load the target to add all messages
            this.message_list = document.getElementById("message-list");
        }
        this.add = function(message){
            console.log("New message received")
            console.log(message)
            // Create the new div message
            var div = this.template.cloneNode(true);

            // Sets all the data

            div.dataset['json'] = JSON.stringify(message);
            div.querySelector('.content').innerHTML = message.content
            div.querySelector(".created_at").innerHTML = this.get_display_date(message.created_at);
            div.querySelector(".user").innerHTML = this.get_user_name(message.user);
            div.querySelector(".level").innerHTML = message.level.name;

            // Adds to message list
            var first_message = this.first_message();
            if(!this.insert_before){
                console.log("Inserting last")
                this.message_list.appendChild(div)
            }else{
                console.log("Inserting first")
                this.message_list.insertBefore(div, first_message);
            }

            // Remove past messages
            var messages = this.message_list.querySelectorAll('.message')
            if(messages.length > 10){
                console.info("Clearing "+(messages.length-10)+" past messages")
                for(var i=9;i<messages.length;i++){
                    messages[i].remove();
                }
            }
        }

        this.get_display_date = function(date_str){
            var date = moment(Date.parse(date_str));
            if(date.isBefore(moment().subtract(3, 'h'))){
                return date.format('lll');
            }else{
                return date.fromNow();
            }
            window.bla = date
        }

        this.get_message_data = function(div){
            // Returns all data from a message div
            return JSON.parse(div.dataset['json']);
        }

        this.first_message = function(){
            // Returns the most recent message
            return this.message_list.querySelector('.message');
        }

        this.get_user_name = function(user_obj){
            if(!user_obj.first_name){
                return user_obj.username;
            }else{
                return user_obj.firstname + ' ' + user_obj.lastname;
            }
        }

        this.update_time = function(){
            var messages = this.message_list.querySelectorAll('.message');
            var div;
            var data;
            console.debug('Formating dates')
            for(var i=0;i<messages.length;i++){
                div = messages[i];
                data = this.get_message_data(div);
                div.querySelector(".created_at").innerHTML = this.get_display_date(data.created_at);
            }
        }
    }

    var Messenger = function(){
        // This class handles requests and response from server
        this.init = function(){
            console.log("Initializing messenger system")
            this.message = new Message();
            this.message.init();

            this.last_datetime = null;
            console.log(this.message)

            console.log("Loading first messages");
            this.load();

            // Updates timestamps every minute
            function interval(obj){
                $this = obj;
                return function(){
                    $this.message.update_time()
                }
            }
            setInterval(interval(this), 30000);
        }

        this.getURL = function(){
            var url = window.message_list_url+'?_='+(Math.random());
            if(this.last_datetime){
                url += '&since='+this.last_datetime;
            }else{
                url += '&limit=10';
            }
            url += '&format=json';
            return url;
        }

        this.load = function(url){
            var request = new XMLHttpRequest();
            request.addEventListener('load', this.parseResponse());
            request.open('GET', url?url:this.getURL(), true);
            console.log('Loading messages since '+(this.last_datetime?this.last_datetime:'ever'));
            request.send();
        }

        this.parseResponse = function(){
            var $this = this;
            return function(event){
                console.log('Response received');
                var data = JSON.parse(event.target.response);
                console.log('Number of new messages: '+data.count);
                if(data.count > 0){
                    // If it comes more than 1 and there is messages loaded already, then we must put it in reverse order
                    var start = 0;
                    var end = data.results.length;
                    var pos;
                    for(var i=0;i<end;i++){
                        pos = $this.message.insert_before?end-i-1:i;
                        $this.message.add(data.results[pos]);
                    }
                    var div = $this.message.first_message();
                    var message = $this.message.get_message_data(div);
                    $this.last_datetime = message.created_at;
                    $this.message.insert_before = true;
                }
                if(data.next){
                    $this.load(data.next);
                }else{
                    // Start a timeout to update from the most recent message every 3 seconds
                    // TODO: Change do socket.io or equivalent
                    setTimeout(function(){
                        $this.load();
                    }, 3000);
                }
            }
        }
    }

    system = new Messenger();
    system.init();
})();