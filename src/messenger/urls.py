# -*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url
from django.conf import settings
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.views.generic.base import TemplateView
from rest_framework import routers
from messenger.views import MessageAPIView, GroupsAPIView, LevelAPIView, HomeView, RegisterView

admin.autodiscover()

router = routers.DefaultRouter()
router.register('messages', MessageAPIView)
router.register('groups', GroupsAPIView)
router.register('levels', LevelAPIView)

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', HomeView.as_view(), name='home'),
    url(r'^register/$', RegisterView.as_view(), name='register'),
    url(r'^register/success/$', TemplateView.as_view(template_name='register-success.html'), name='register-success'),
    url(r'^login/$', 'django.contrib.auth.views.login', {'template_name': 'login.html'}, name='login'),
    url(r'^logout/$', 'django.contrib.auth.views.logout', name='logout'),
    url(r'^', include(router.urls)),
)

if settings.DEBUG:
    urlpatterns += staticfiles_urlpatterns()