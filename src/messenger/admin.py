# -*- coding: utf-8 -*-

from django.contrib import admin
from messenger.models import Level, Message
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token

class LevelAdmin(admin.ModelAdmin):
    list_display = ('name', 'priority', 'is_active')
    list_filter = ('is_active',)
    
    
class MessageAdmin(admin.ModelAdmin):
    list_display = ('content', 'level', 'is_public', 'target', 'created_at')
    list_filter = ('group_target', 'level', 'is_public')
    filter_horizontal = ('group_target',)
    def target(self, obj):
        '''Return the name of the groups for any given message'''
        return ', '.join([g.name for g in obj.group_target.all()])

class TokenInline(admin.TabularInline):
    model = Token
    can_add = False
    can_delete = False
    

class UserProfileAdmin(UserAdmin):
    filter_horizontal = ('user_permissions', 'groups')
    list_display = ('username', 'email', 'is_staff', 'token')
    inlines = [TokenInline]
    
    def token(self, obj):
        return obj.auth_token


admin.site.register(Level, LevelAdmin)
admin.site.register(Message, MessageAdmin)

admin.site.unregister(User)
admin.site.register(User, UserProfileAdmin)    