# -*- coding: utf-8 -*-

from django.contrib.auth.models import User
from django.contrib.auth.models import User, Group
from django.db import models
from django.db.models.signals import post_save
import uuid
from django.core.exceptions import ValidationError
from rest_framework.authtoken.models import Token


class Level(models.Model):
    is_active = models.BooleanField(default=True, db_index=True)
    name = models.CharField(max_length=20, db_index=True, unique=True)
    priority = models.IntegerField(default=0)
    
    class Meta:
        ordering = ('priority', 'name')

    def __unicode__(self):
        return self.name
    

class Message(models.Model):
    user = models.ForeignKey(User, related_name='messages')
    level = models.ForeignKey(Level, related_name='messages')
    group_target = models.ManyToManyField(Group, related_name='messages')
    content = models.TextField()
    is_public = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True, db_index=True)

    # TODO: Either is_public or group_target must be set
    class Meta:
        ordering = ('-created_at','-id')

    def __unicode__(self):
        return self.content
    
    

def create_user_token(sender, instance, created, **kwargs):
    if created:
        Token.objects.create(user=instance)

post_save.connect(create_user_token, sender=User)
    